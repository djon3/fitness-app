package com.example.nathan.fitnesstracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.View;
import android.widget.Chronometer;

/**
 * Created by djon on 11/25/2014.
 */
public class TimerActivity extends Activity {
    private Chronometer chronometer;
    private long lastPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_timer);
       super.onCreate(savedInstanceState);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
      }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }



    public void onButtonClick(View view) {

        Intent i;
        switch (view.getId()) {

            case R.id.buttonStart:
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                break;
            case R.id.buttonStop:
                lastPause = SystemClock.elapsedRealtime();
                chronometer.stop();
                break;
            case R.id.buttonResume:
                chronometer.setBase(chronometer.getBase() + SystemClock.elapsedRealtime() - lastPause);
                chronometer.start();
                break;
        }
    }
}