package com.example.nathan.fitnesstracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class ExerciseEditorActivity extends Activity {

    CheckBox ch;
    int exItemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_editor);

        //Populate fields
        Intent i = getIntent();

        Exercise e = (Exercise) i.getSerializableExtra("exercise");


        if (e != null) {
            exItemId = e.getId();

            EditText etName = (EditText) findViewById(R.id.etExerciseName);
            etName.setText(e.getName());

            CheckBox cbRepsMsg = (CheckBox) findViewById(R.id.cbReps);
            cbRepsMsg.setChecked(e.getReps() == 1);

            CheckBox cbWeightMsg = (CheckBox) findViewById(R.id.cbWeight);
            cbWeightMsg.setChecked(e.getWeight() == 1);

            CheckBox cbTimeMsg = (CheckBox) findViewById(R.id.cbTime);
            cbTimeMsg.setChecked(e.getTime() == 1);

            EditText etDesc = (EditText) findViewById(R.id.etDescription);
            etDesc.setText(e.getDescription());
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exercise_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void createSave() {

        try {

            Exercise ex = new Exercise();
            // get user's text input
            EditText etName = (EditText) findViewById(R.id.etExerciseName);
            ex.setName(etName.getText().toString());
            EditText etDescription = (EditText) findViewById(R.id.etDescription);
            if (etDescription.getText().toString().isEmpty())
                Toast.makeText(getApplicationContext(), "Enter an exercise name...", Toast.LENGTH_LONG).show();
            else
                ex.setDescription(etDescription.getText().toString());

            //get user's checkbox input
            //Reps
            ch = (CheckBox) findViewById(R.id.cbReps);
            if (ch.isChecked()) {
                ex.setReps(1);
            } else {
                ex.setReps(0);
            }
            //Weight
            ch = (CheckBox) findViewById(R.id.cbWeight);
            if (ch.isChecked()) {
                ex.setWeight(1);
            } else {
                ex.setWeight(0);
            }

            //Time
            ch = (CheckBox) findViewById(R.id.cbTime);
            if (ch.isChecked()) {
                ex.setTime(1);
            } else {
                ex.setTime(0);
            }


            ExercisesDBAdapter db = new ExercisesDBAdapter(this);
            db.open();

            if (exItemId == 0) {
                db.insertExercise(ex);
            } else {
                ex.setId(exItemId);
                db.updateExercise(ex);
            }

            db.close();

            Toast.makeText(getApplicationContext(), "Saved!", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            System.out.println("Error saving: " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Problem saving...", Toast.LENGTH_LONG).show();
        }
    }


    public void onButtonClick(View view) {

        switch (view.getId()) {
            case R.id.buttonCreateSave:
                createSave();
                finish();
                break;
            case R.id.buttonCreateCancel:
                finish();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Unknown call to onButtonClick", Toast.LENGTH_LONG).show();
                break;
        }
    }
}