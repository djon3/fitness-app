package com.example.nathan.fitnesstracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
WorkoutViewActivity
Purpose: Displays all exercises in the current workout. Can add and remove exercises.
*/

public class WorkoutViewerActivity extends Activity {

    private ListView lvWorkoutExercisesItems;
    private ArrayAdapter<String> listAdapter;
    private WorkoutDBAdapter db = new WorkoutDBAdapter(this);
    private AllWorkoutsDBAdapter awdb = new AllWorkoutsDBAdapter(this);
    private Workout w = new Workout();
    private Workout workout = new Workout();
    private ArrayList<Workout> workoutArray;
    private int workoutId;
    private ByteArrayOutputStream bos = new ByteArrayOutputStream();
    private ObjectInput in = null;
    private ObjectOutput out = null;
    private Workout workoutExtra;
    Object exObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_viewer);

        //Populate fields
        Intent i = getIntent();
        workoutId = (Integer) i.getSerializableExtra("id");
        workoutExtra = (Workout) i.getSerializableExtra("workout");

        if (workoutId != 0) {
            populateListViewForNew();
            populateFieldsForUpdate();
            registerClickCallBack();
        }

    }

    public void populateFieldsForUpdate() {


        if (workoutExtra != null) {
            workoutId = workoutExtra.getId();

            EditText workoutName = (EditText) findViewById(R.id.etWorkoutName);
            workoutName.setText(workoutExtra.getName());

            //Deserialize
            byte[] workoutExercise = workoutExtra.getExercises();
            ByteArrayInputStream bis = new ByteArrayInputStream(workoutExercise);
            exObject = new Object();
            try {
                try {
                    in = new ObjectInputStream(bis);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    try {
                        exObject = in.readObject();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } finally {
                try {
                    bis.close();
                } catch (IOException ex) {
                    // ignore close exception
                }
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ex) {
                    // ignore close exception
                }

            }

            //populate with exercises
            awdb.open();
            String[] workouts = (String[]) exObject;

            if (workouts != null) {
                List<String> str = new ArrayList<String>();
                Collections.addAll(str, workouts);

                listAdapter = new ArrayAdapter<String>(this, R.layout.exercise_item, str);
                lvWorkoutExercisesItems = (ListView) findViewById(R.id.lvWorkoutExercises);
                lvWorkoutExercisesItems.setAdapter(listAdapter);

            }
            awdb.close();

        }
    }

    public void populateListViewForNew() {
        db.open();
        ArrayList<Workout> workouts = db.getAllWorkouts();
        if (!workouts.isEmpty()) {
            List<String> str = new ArrayList<String>();
            for (Workout w : workouts) {
                str.add(w.getName());

            }

            listAdapter = new ArrayAdapter<String>(this, R.layout.exercise_item, str);
            lvWorkoutExercisesItems = (ListView) findViewById(R.id.lvWorkoutExercises);
            lvWorkoutExercisesItems.setAdapter(listAdapter);

        }
        db.close();

    }


    private void registerClickCallBack() {

        try {
            lvWorkoutExercisesItems = (ListView) findViewById(R.id.lvWorkoutExercises);
            lvWorkoutExercisesItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    awdb.open();
                    db.open();

                    workoutArray = db.getAllWorkouts();
                    w = workoutArray.get(position);

                    workout.setId(w.getId());
                    workout.setName(w.getName());

                    db.close();
                    awdb.close();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error...", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.workout_viewer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void removeExerciseFromWorkout() {

        db.open();
        awdb.open();
        if (db.deleteWorkout(workout.getId())) {
            Toast.makeText(this, "Delete successful.", Toast.LENGTH_LONG).show();
            listAdapter.clear();
            populateListViewForNew();
            listAdapter.notifyDataSetChanged();
        } else
            Toast.makeText(this, "Delete failed.", Toast.LENGTH_LONG).show();


        awdb.close();
        db.close();

    }

    public void saveEntireWorkout() {
        try {

            Workout work = new Workout();
            // get user's text input
            EditText workoutName = (EditText) findViewById(R.id.etWorkoutName);
            if (workoutName.getText().toString().isEmpty())
                Toast.makeText(getApplicationContext(), "Enter a workout name...", Toast.LENGTH_LONG).show();
            else
                work.setName(workoutName.getText().toString());


            String[] exItems = new String[listAdapter.getCount()];
            for (int i = 0; i < listAdapter.getCount(); ++i) {
                View v = lvWorkoutExercisesItems.getChildAt(i);
                TextView tv = (TextView) v.findViewById(R.id.listItems);
                exItems[i] = (String) tv.getText();
            }
            //Serialize
            try {
                out = new ObjectOutputStream(bos);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                out.writeObject(exItems);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byte[] exBytes = bos.toByteArray();

            work.setExercises(exBytes);

            awdb.open();

            if (workoutId == 0) {
                awdb.insertWorkout(work);
            } else {
                work.setId(workoutId);
                awdb.updateWorkout(work);
            }


            awdb.close();

        } catch (Exception e) {
            System.out.println("Error saving: " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Problem saving: must have at least one exercise.", Toast.LENGTH_LONG).show();
        }
    }

    public void onButtonClick(View view) {
        Intent i;
        try {
            switch (view.getId()) {

                case R.id.buttonViewerAdd:
                    i = new Intent(this, ExerciseSelectorActivity.class);
                    int result = 0;
                    startActivityForResult(i, result);
                    break;
                case R.id.buttonViewerRemove:
                    removeExerciseFromWorkout();
                    break;
                case R.id.buttonViewerDone:
                    saveEntireWorkout();
                    finish();
                    break;
                default:
                    Toast.makeText(getApplicationContext(), "Unknown call to onButtonClick", Toast.LENGTH_LONG).show();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error...", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        populateListViewForNew();
        populateFieldsForUpdate();
        registerClickCallBack();
    }


}
