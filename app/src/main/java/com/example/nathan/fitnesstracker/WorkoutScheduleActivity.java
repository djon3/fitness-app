package com.example.nathan.fitnesstracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WorkoutScheduleActivity extends NavigationActivity {

    private ScheduleDBAdapter scheduleDB = new ScheduleDBAdapter(this);
    private WorkoutDBAdapter workoutDB = new WorkoutDBAdapter(this);

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    String currentDate = sdf.format(new Date());
    String selectedDate;

    ArrayList<Workout> workouts = new ArrayList<Workout>();
    Workout currentWorkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_workout_schedule);
        super.onCreate(savedInstanceState);
        super.setCurrentActivity(WorkoutScheduleActivity.class);

//        scheduleDB.open();
//        scheduleDB.insertInitialWorkoutDay();
//        scheduleDB.close();

        workouts.clear();
        workoutDB.open();
        workouts = workoutDB.getAllWorkouts();
        workoutDB.close();

        CalendarView v = (CalendarView) findViewById(R.id.workoutCalendarView);
        v.setShowWeekNumber(false);
        v.setOnDateChangeListener( new CalendarView.OnDateChangeListener() {
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {


                //getResources().getColor(R.color.abc_search_url_text_selected)
                currentDate = sdf.format(new Date()); //date today

                //months start from 0 for some reason so add +1
                month = month + 1;

                setWorkoutForSelectedDay( year +"-"+ month +"-"+ dayOfMonth );

                //Toast.makeText(getApplicationContext(),dayOfMonth+ "/"+month+"/"+year+currentDate,Toast.LENGTH_LONG).show();
            }
        });

        setWorkoutForSelectedDay(currentDate);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.workout_schedule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent i = new Intent(this,AboutActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setWorkoutForSelectedDay(String date)
    {
        Button btnRecordYourProgress = (Button) findViewById(R.id.buttonRecordYourProgress);
        TextView textViewCurrentWorkout = (TextView) findViewById(R.id.textViewCurrentWorkout);

        scheduleDB.open();
        //if(c.moveToFirst())
        Cursor c = scheduleDB.getWorkoutDay( date ); //dd-mm-yyyy

        //c.getInt(1); // workoutid
        //search for matching workout ID; will be moved to a join query when/if we can make the database more robust
        if(c.moveToFirst()) {
            for (Workout w : workouts) {
                if (w.getId() == c.getInt(1)) {
                    currentWorkout = new Workout();
                    currentWorkout = w;
                    break;
                }
            }
        }
        else {
            currentWorkout = new Workout();
            currentWorkout.setId(0);
            currentWorkout.setName("Rest Day");
        }
        scheduleDB.close();

        selectedDate = date;




        if(currentDate.equalsIgnoreCase(selectedDate))
        {
            //btnRecordYourProgress.setEnabled(true); //user may want to do a workout they missed, etc, so commenting this out
            textViewCurrentWorkout.setText(currentWorkout.getName() + " Today!  --  ");
        }
        else
        {
            //btnRecordYourProgress.setEnabled(false);
            textViewCurrentWorkout.setText(currentWorkout.getName() + "  --  ");
        }
    }

    private void removeWorkoutForSelectedDay()
    {
        scheduleDB.open();
        if (scheduleDB.deleteWorkoutDay(selectedDate)) {
            Toast.makeText(this, "Workout removed.", Toast.LENGTH_LONG).show();
        } else
            Toast.makeText(this, "There was a problem removing the workout from the calendar.", Toast.LENGTH_LONG).show();
        scheduleDB.close();
        //refresh
        setWorkoutForSelectedDay(selectedDate);
    }

    protected void showInputDialog()
    {

        final AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Pick a workout for the selected day:");

        String[] strArray = new String[workouts.size()];
        if (!workouts.isEmpty()) {
            for (int i = 0; i < workouts.size(); ++i) {
                strArray[i] = workouts.get(i).getName();
            }

        }

        b.setItems(strArray, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                try {
                    scheduleDB.open();
                    scheduleDB.insertWorkoutDay(workouts.get(which), selectedDate);
                    scheduleDB.close();

                    CalendarView v = (CalendarView) findViewById(R.id.workoutCalendarView);
                    v.setSelected(true);


                    //refresh
                    Toast.makeText(getApplicationContext(), "Added to schedule successfully.", Toast.LENGTH_LONG).show();
                }
                catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Error saving to schedule.", Toast.LENGTH_LONG).show();
                }
            }

        });

        b.show();

    }



    public void onButtonClick(View view)
    {
        Intent i;
        switch(view.getId())
        {
            case R.id.buttonRecordYourProgress:
                // A variant of this activity with a filter of exercises tied to a workout will
                // be needed as users will go back and forth between it and the recording activity.
                i = new Intent(this,TimerActivity.class);
                startActivity(i);
                break;
            case R.id.buttonAddToCalendar:
                showInputDialog();
                break;
            case R.id.buttonRemoveFromCalendar:
                removeWorkoutForSelectedDay();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Unknown call to onButtonClick", Toast.LENGTH_LONG).show();
                break;
        }
    }

}