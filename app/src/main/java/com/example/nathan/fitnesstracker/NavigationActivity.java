package com.example.nathan.fitnesstracker;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by jordon on 2014-10-24.
 */
public class NavigationActivity extends Activity {

    public Class currentActivity;
    public DrawerLayout drawerLayout;
    public ListView drawerListMain;
    public ListView drawerListMisc;
    public String[] mainNavItems;
    public String[] miscNavItems;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("locate")) {
            setContentView(savedInstanceState.getInt("locate"));
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawerToggle = new ActionBarDrawerToggle((Activity) this, drawerLayout, R.drawable.ic_drawer, 0, 0) {
            public void onDrawerClosed(View view)
            {
                getActionBar().setTitle(R.string.app_name);
            }

            public void onDrawerOpened(View drawerView)
            {
                getActionBar().setTitle(R.string.menu);
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        //First set of list items
        mainNavItems = getResources().getStringArray(R.array.navitems_main);
        drawerListMain = (ListView) findViewById(R.id.main_drawer);
        drawerListMain.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, android.R.id.text1, mainNavItems));

        drawerListMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position) {
                    case 0:
                        launchActivity(HomeActivity.class);
                        break;
                    case 1:
                        launchActivity(ExerciseListActivity.class);
                        break;
                    case 2:
                        launchActivity(WorkoutListActivity.class);
                        break;
                    case 3:
                        launchActivity(WorkoutScheduleActivity.class);
                        break;
                    case 4:
                        launchActivity(LocateActivity.class);
                        break;
                    case 5:
                        launchActivity(TimerActivity.class);
                        break;
                }
            }
        });

        //Second set of list items
        miscNavItems = getResources().getStringArray(R.array.navitems_misc);
        drawerListMisc = (ListView) findViewById(R.id.misc_drawer);
        drawerListMisc.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, android.R.id.text1, miscNavItems));

        drawerListMisc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        launchActivity(AboutActivity.class);
                        break;
                    case 1:
                        finish();
                        break;
                }
            }
        });
    }

    public void launchActivity(Class activity) {

        if (currentActivity != activity) {
            Intent i = new Intent(getApplication(), activity);
            startActivity(i);

            if (currentActivity != HomeActivity.class) {
                finish();
            }
        }

    }

    public void setCurrentActivity(Class activity) {
        currentActivity = activity;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

}
