package com.example.nathan.fitnesstracker;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/*
WorkoutListActivity
Purpose: Displays all workouts in a listview. Uses CRUD
*/

public class WorkoutListActivity extends NavigationActivity {

    private ListView lvWorkoutItems;
    private ArrayAdapter<String> listAdapter;
    private AllWorkoutsDBAdapter db = new AllWorkoutsDBAdapter(this);
    private Workout w = new Workout();
    private Workout workout = new Workout();
    private ArrayList<Workout> workoutArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_workout_list);
        super.onCreate(savedInstanceState);
        super.setCurrentActivity(WorkoutListActivity.class);

        populateListView();
        registerClickCallBack();
    }


    public void populateListView() {
        db.open();
        ArrayList<Workout> workouts = db.getAllWorkouts();
        if (!workouts.isEmpty()) {
            List<String> str = new ArrayList<String>();
            for (Workout w : workouts) {
                str.add(w.getName());
            }

            listAdapter = new ArrayAdapter<String>(this, R.layout.exercise_item, str);
            lvWorkoutItems = (ListView) findViewById(R.id.listWorkouts);
            lvWorkoutItems.setAdapter(listAdapter);

        }
        db.close();

    }

    private void registerClickCallBack() {

        lvWorkoutItems = (ListView) findViewById(R.id.listWorkouts);
        lvWorkoutItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                db.open();
                workoutArray = db.getAllWorkouts();
                w = workoutArray.get(position);
                workout.setId(w.getId());
                workout.setName(w.getName());
                workout.setExercises(w.getExercises());
                db.close();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_workout_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    public void removeWorkout() {
        db.open();

        if (db.deleteWorkout(w.getId())) {
            Toast.makeText(this, "Delete successful.", Toast.LENGTH_LONG).show();
            listAdapter.clear();
            populateListView();
            listAdapter.notifyDataSetChanged();
        } else
            Toast.makeText(this, "Delete failed.", Toast.LENGTH_LONG).show();
        db.close();
    }

    public void onButtonClick(View view) {

        Intent i;
        switch (view.getId()) {

            case R.id.addNewWorkout:
                i = new Intent(this, WorkoutViewerActivity.class);
                int id = 0;
                i.putExtra("id", id);
                int result = 0;
                startActivityForResult(i, result);
                break;
            case R.id.updateWorkout:
                i = new Intent(this, WorkoutViewerActivity.class);
                i.putExtra("id", workout.getId());
                i.putExtra("workout", workout);
                result = 0;
                startActivityForResult(i, result);
                break;
            case R.id.removeWorkout:
                removeWorkout();
                break;
            case R.id.doneWorkout:
                finish();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Unknown call to onButtonClick", Toast.LENGTH_LONG).show();
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        populateListView();
        registerClickCallBack();
    }


}
