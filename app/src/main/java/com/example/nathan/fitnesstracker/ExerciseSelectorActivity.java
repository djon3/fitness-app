package com.example.nathan.fitnesstracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/*
ExerciseSelectorActivity
Purpose: activity that displays all exercies in a spinner and allows the user to add them to the certain workout.
 */

public class ExerciseSelectorActivity extends Activity {

    private Spinner spinItems;
    private ExercisesDBAdapter db = new ExercisesDBAdapter(this);
    private WorkoutDBAdapter wdb = new WorkoutDBAdapter(this);
    private Exercise exercise = new Exercise();
    private ArrayList<Exercise> exerciseArray;
    private Exercise e = new Exercise();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_selector);

        populateSpinner();
        registerClickCallBack();


    }

    private void populateSpinner() {
        db.open();
        ArrayList<Exercise> exercises = db.getAllExercises();
        if (!exercises.isEmpty()) {

            List<String> str = new ArrayList<String>();
            for (Exercise e : exercises) {
                str.add(e.getName());
            }
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.exercise_item, str);
            spinItems = (Spinner) findViewById(R.id.spinnerExercises);
            spinItems.setAdapter(spinnerAdapter);
        }
        db.close();

    }

    private void registerClickCallBack() {

        spinItems = (Spinner) findViewById(R.id.spinnerExercises);

        spinItems.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                db.open();
                exerciseArray = db.getAllExercises();

                e = exerciseArray.get(position);

                exercise.setId(e.getId());
                exercise.setName(e.getName());
                exercise.setReps(e.getReps());
               exercise.setWeight(e.getWeight());
               exercise.setTime(e.getTime());
               exercise.setDescription(e.getDescription());


                TextView tvDesc = (TextView) findViewById(R.id.tvDescription);
                if (!e.getDescription().isEmpty())
                    tvDesc.setText(e.getDescription());

                db.close();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exercise_selector, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void saveExerciseToWorkout() {
        try {
            wdb.open();
            wdb.insertWorkout(exercise);
            wdb.close();
        } catch (Exception e) {
            System.out.println("Error saving: " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Problem saving...", Toast.LENGTH_LONG).show();
        }
    }

    public void onButtonClick(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.buttonSelectorAdd:
                saveExerciseToWorkout();
                finish();
                break;
            case R.id.buttonSelectorNew:
                i = new Intent(this, ExerciseEditorActivity.class);
                int result = 0;
                startActivityForResult(i, result);
                break;
            case R.id.buttonSelectorDone:
                setResult(RESULT_CANCELED);
                finish();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Unknown call to onButtonClick", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        populateSpinner();
        registerClickCallBack();
    }

}
