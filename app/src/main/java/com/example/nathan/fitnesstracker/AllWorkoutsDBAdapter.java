package com.example.nathan.fitnesstracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/*
AllWorkoutsDBAdapter
Purpose: Database adapter for exercises in the current workout.
*/

public class AllWorkoutsDBAdapter {

    static final String KEY_ROWID = "_id";
    static final String KEY_NAME = "name";
    static final String KEY_EXERCISES = "exercises";
    static final String TAG = "DBAdapter";

    static final String DATABASE_NAME = "AllWDB";
    static final String DATABASE_TABLE = "allWorkouts";
    static final int DATABASE_VERSION = 1;

    static final String DATABASE_CREATE =
            "create table allWorkouts (_id integer primary key autoincrement, " +
                    "name text not null, " +
                    "exercises byte[] ); ";
    final Context context;

    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    public AllWorkoutsDBAdapter(Context ctx)
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);

    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {

            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS contacts");
            onCreate(db);
        }
    }

    //---opens the database---
    public AllWorkoutsDBAdapter open() throws SQLException
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---
    public void close()
    {
        DBHelper.close();
    }

    //---insert an workout into the database---

    public long insertWorkout(Workout w)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, w.getName());
        initialValues.put(KEY_EXERCISES, w.getExercises());
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    //---deletes a particular workout---
    public boolean deleteWorkout(long rowId)
    {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }
    //---deletes a particular exercise from workout---
    public boolean deleteWorkoutExercise(String[] exName)
    {
        return db.delete(DATABASE_TABLE, KEY_EXERCISES + "=" + exName, null) > 0;
    }

    //---retrieves all the workouts---
    public ArrayList<Workout> getAllWorkouts()
    {
        Cursor c = db.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME, KEY_EXERCISES },
                null, null, null, null, null);
        ArrayList<Workout> workouts = new ArrayList<Workout>();
        if (c.moveToFirst()) {
            do {
                Workout w = new Workout();
                w.setId(c.getInt(0));
                w.setName(c.getString(1));
                w.setExercises(c.getBlob(2));
                workouts.add(w);

            } while (c.moveToNext());
        }

        return workouts;
    }



    //---updates a workout---
    public boolean updateWorkout(Workout w)
    {
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, w.getName());
        args.put(KEY_EXERCISES, w.getExercises());
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + w.getId(), null) > 0;
    }

}
