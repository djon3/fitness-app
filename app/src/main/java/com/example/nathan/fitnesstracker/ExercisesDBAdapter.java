package com.example.nathan.fitnesstracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
/*
WorkoutDBAdapter
Purpose: Database adapter for working with exercise items in the current workout.
 */

public class ExercisesDBAdapter {


    static final String KEY_ROWID = "_id";
    static final String KEY_NAME = "name";
    static final String KEY_REPS = "reps";
    static final String KEY_WEIGHT = "weight";
    static final String KEY_TIME = "time";
    static final String KEY_DESCRIPTION = "description";
    static final String TAG = "DBAdapter";

    static final String DATABASE_NAME = "ExerciseDataBase";
    static final String DATABASE_TABLE = "exercises";
    static final int DATABASE_VERSION = 1;

    static final String DATABASE_CREATE =
            "create table exercises (_id integer primary key autoincrement, " +
                    "name text not null, " +
                    "reps int, " +
                    "weight int, " +
                    "time int, " +
                    "description text); ";

    static final String DATABASE_INSERT1 = "insert into exercises (name, reps, weight, time, description) " +
            "values ('Situps', 1, 0, 0, 'A set of standard situps.');";
    static final String DATABASE_INSERT2 = "insert into exercises (name, reps, weight, time, description) " +
            "values ('Pushups', 1, 0, 0, 'A set of standard Pushups.');";
    static final String DATABASE_INSERT3 = "insert into exercises (name, reps, weight, time, description) " +
            "values ('8 Min Mile', 0, 0, 1, 'Run a mile in 8 minutes or less.');";

    static final String DATABASE_DELETE_INITIAL = "delete from exercises where name = 'Situps' or name = 'Pushups' or name = '8 Min Mile'";

    final Context context;

    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    public ExercisesDBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);

    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS contacts");
            onCreate(db);
        }
    }

    //---opens the database---
    public ExercisesDBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---
    public void close() {
        DBHelper.close();
    }

    //---insert an exercise into the database---

    public void insertInitialExercises() {
        db.execSQL(DATABASE_DELETE_INITIAL);
        db.execSQL(DATABASE_INSERT1);
        db.execSQL(DATABASE_INSERT2);
        db.execSQL(DATABASE_INSERT3);
    }

    public long insertExercise(Exercise e) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, e.getName());
        initialValues.put(KEY_REPS, e.getReps());
        initialValues.put(KEY_WEIGHT, e.getWeight());
        initialValues.put(KEY_TIME, e.getTime());
        initialValues.put(KEY_DESCRIPTION, e.getDescription());
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    //---deletes a particular contact---
    public boolean deleteExercise(long rowId) {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    //---retrieves all the contacts---
    public ArrayList<Exercise> getAllExercises() {
        Cursor c = db.query(DATABASE_TABLE, new String[]{KEY_ROWID, KEY_NAME,
                        KEY_REPS, KEY_WEIGHT, KEY_TIME, KEY_DESCRIPTION},
                null, null, null, null, null);
        ArrayList<Exercise> exercises = new ArrayList<Exercise>();
        if (c.moveToFirst()) {
            do {
                Exercise ex = new Exercise();
                ex.setId(c.getInt(0));
                ex.setName(c.getString(1));
                ex.setReps(c.getInt(2));
                ex.setWeight(c.getInt(3));
                ex.setTime(c.getInt(4));
                ex.setDescription(c.getString(5));
                exercises.add(ex);

            } while (c.moveToNext());
        }

        return exercises;
    }


    //---updates a exercise---
    //public boolean updateExercise(long rowId, String name, int reps, int weight, int time, String description)
    public boolean updateExercise(Exercise e) {
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, e.getName());
        args.put(KEY_REPS, e.getReps());
        args.put(KEY_WEIGHT, e.getWeight());
        args.put(KEY_TIME, e.getTime());
        args.put(KEY_DESCRIPTION, e.getDescription());
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + e.getId(), null) > 0;
    }

}

