package com.example.nathan.fitnesstracker;


import java.io.Serializable;

/**
 * Workout
 * Purpose: getters and setters for the workout
 * Created on 10/24/2014.
 */
public class Workout implements Serializable {

    private int id;
    private String name;
    private byte[] exercises;

    public Workout() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getExercises() {  return exercises;    }

    public void setExercises(byte[] exercises) {
        this.exercises = exercises;
    }

}
