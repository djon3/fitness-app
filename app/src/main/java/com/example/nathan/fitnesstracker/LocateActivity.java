package com.example.nathan.fitnesstracker;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class LocateActivity extends NavigationActivity implements AsyncGetGymsListener<String> {

    private GoogleMap mMap;
    private Location mCurrentLocation;
    private Marker mCurrentMarker;
    private int mGymIcon;
    private int mHomeIcon;
    private ArrayList<GymInfo> mGyms = new ArrayList<GymInfo>();
    private ArrayList<Marker> mGymMarkers = new ArrayList<Marker>();
    private boolean mManualMove;

    private static final String PLACES_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
    private static final String PLACES_KEY = "AIzaSyATAz4Zi2av7206I5JFWqCBUzbzlpnLcdA";
    private static final int PLACES_RADIUS = 2500;

    private static final int MAP_ZOOM = 13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        savedInstanceState = new Bundle();
        int id = R.layout.activity_locate;
        savedInstanceState.putInt("locate", id);
        super.onCreate(savedInstanceState);
        super.setCurrentActivity(LocateActivity.class);

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {

            mHomeIcon = R.drawable.home;
            mGymIcon = R.drawable.weights;
            setUpMapIfNeeded();

        } else {
            Toast.makeText(getApplication(), "Sorry Maps are not available", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void setupSpinner() {

        Spinner spinnerGyms = (Spinner) findViewById(R.id.locate_gyms_spinner);
        spinnerGyms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GymInfo selected = mGyms.get(position);

                if (!mManualMove) {
                    mMap.moveCamera(CameraUpdateFactory.zoomTo(MAP_ZOOM));
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(selected.getLocation()), 1500, null);
                }
                mManualMove = false;

                Marker m = mGymMarkers.get(position);
                m.showInfoWindow();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void populateSpinner() {
        Spinner spinnerGyms = (Spinner) findViewById(R.id.locate_gyms_spinner);
        spinnerGyms.setAdapter(null);
        if (!mGyms.isEmpty()) {

            List<String> str = new ArrayList<String>();
            for (GymInfo g : mGyms) {
                str.add(g.getName());
            }
            spinnerGyms.setAdapter(new ArrayAdapter<String>(this, R.layout.gym_item, str));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                getCurrentLocation();
                return true;
            }
        });
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mCurrentLocation.setLatitude(latLng.latitude);
                mCurrentLocation.setLongitude(latLng.longitude);
                updateGymCount(0);
                mGyms.clear();
                mManualMove = true;
                findGyms(mCurrentLocation);
            }
        });
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getLayoutInflater().inflate(R.layout.marker_infowindow, null);
                LatLng latlng = marker.getPosition();

                TextView tvName = (TextView) v.findViewById(R.id.infowindow_name);
                TextView tvLat = (TextView) v.findViewById(R.id.infowindow_lat);
                TextView tvLng = (TextView) v.findViewById(R.id.infowindow_lng);
                TextView tvDetails = (TextView) v.findViewById(R.id.infowindow_details);
                ImageView ivIcon = (ImageView) v.findViewById(R.id.infowindow_icon);

                for (GymInfo g : mGyms) {
                    if (g.getName().equals(marker.getTitle())) {
                        ivIcon.setImageBitmap(g.getIconBitmap());
                    }
                }

                tvName.setText(marker.getTitle());
                tvDetails.setText(marker.getSnippet());
                tvLat.setText("Latitude: " + latlng.latitude);
                tvLng.setText("Longitude: " + latlng.longitude);

                return v;
            }
        });
        setupSpinner();
        getCurrentLocation();
    }

    private void findGyms(Location l) {

        double lat = l.getLatitude();
        double lng = l.getLongitude();

        LatLng loc = new LatLng(lat, lng);

        if (mCurrentMarker != null) {
            mCurrentMarker.remove();
        }

        mMap.moveCamera(CameraUpdateFactory.zoomTo(MAP_ZOOM));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(loc), 3000, null);

        mCurrentMarker = mMap.addMarker(new MarkerOptions()
                .position(loc)
                .title("Start Point")
                .icon(BitmapDescriptorFactory.fromResource(mHomeIcon))
                .snippet("Last requested location"));

        String getGymsURL = PLACES_URL + "location=" + lat + "," + lng
                + "&radius=" + PLACES_RADIUS
                + "&sensor=true&types=gym"
                + "&key=" + PLACES_KEY;

        GetGyms g = new GetGyms(this);
        g.execute(getGymsURL);
    }

    public void onTaskComplete(String result) {

        try {
            JSONObject obj = new JSONObject(result);
            JSONArray gyms = obj.getJSONArray("results");

            for (Marker m : mGymMarkers) {
                if (m != null) {
                    m.remove();
                }
            }
            mGymMarkers.clear();

            for (int i = 0; i < gyms.length(); i++) {
                JSONObject gym = gyms.getJSONObject(i);
                GymInfo g = new GymInfo();
                g.parseJSON(gym, i);
                addGymMarker(g);

                new GetGymIcon(g).execute();

                mGyms.add(g);
            }

            if (mGyms.size() > 0) {
                updateGymCount(mGyms.size());
                populateSpinner();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateGymCount(int size) {
        TextView gymCount = (TextView) findViewById(R.id.locate_gyms_found_text);
        gymCount.setText(size + " Gyms found");
    }

    public void addGymMarker(GymInfo g) {

        Marker gymMarker = mMap.addMarker(new MarkerOptions()
                .position(g.getLocation())
                .title(g.getName())
                .icon(BitmapDescriptorFactory.fromResource(mGymIcon))
                .snippet(g.extrasToString()));

        mGymMarkers.add(gymMarker);
    }

    public void getCurrentLocation() {
        LocationManager locMan = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        mCurrentLocation = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        LatLng loc = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        mMap.animateCamera(CameraUpdateFactory.newLatLng(loc), 3000, null);
        findGyms(mCurrentLocation);
    }

    public void onHelpClick(View view) {
        Toast.makeText(getApplication(), "Long press for a new location!", Toast.LENGTH_SHORT).show();
    }
}
