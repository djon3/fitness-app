package com.example.nathan.fitnesstracker;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by jordon on 2014-10-25.
 */
public class GymInfo implements Serializable {

    private LatLng location;
    private String icon;
    private Bitmap iconBitmap;
    private String name;
    private boolean isOpen;
    private String address;
    private String rating;

    public GymInfo(String name, LatLng location, String address) {
        this.name = name;
        this.location = location;
        this.address = address;
    }

    public GymInfo() {

    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRating() { return rating; }

    public void setRating(String rating) { this.rating = rating; }

    public Bitmap getIconBitmap() {
        return iconBitmap;
    }

    public void setIconBitmap(Bitmap iconBitmap) {
        this.iconBitmap = iconBitmap;
    }

    public void parseJSON(JSONObject gym, int i) {

        try {
            name = gym.has("name") ? gym.getString("name") : "";
            address = gym.has("vicinity") ? gym.getString("vicinity") : "";
            isOpen = gym.has("opening_hours") ? gym.getJSONObject("opening_hours").getBoolean("open_now") : false;
            icon = gym.has("icon") ? gym.getString("icon") : "";
            rating = gym.has("rating") ? gym.getString("rating") : "";

            if (gym.has("geometry")) {
                LatLng gymLoc = new LatLng(
                        gym.getJSONObject("geometry")
                                .getJSONObject("location")
                                .getDouble("lat"),
                        gym.getJSONObject("geometry")
                                .getJSONObject("location")
                                .getDouble("lng"));
                location = gymLoc;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String extrasToString() {

        StringBuilder ss = new StringBuilder();
        if (!address.isEmpty()){
            ss.append(address + "\n");
        }

        if (isOpen) {
            ss.append("Open \n");
        } else {
            ss.append("Closed \n");
        }

        if (!rating.isEmpty()) {
            ss.append("Rating: " + rating + " \n");
        }

        return ss.toString();
    }
}
