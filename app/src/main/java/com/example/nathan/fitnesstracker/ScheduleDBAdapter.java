package com.example.nathan.fitnesstracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Nathan on 24/10/2014.
 */
public class ScheduleDBAdapter {

    static final String KEY_ROWID = "_id";
    static final String KEY_WORKOUTID = "workoutid";
    static final String KEY_DATE = "date";
    static final String TAG = "DBAdapter";

    static final String DATABASE_NAME = "WorkoutScheduleDatabase";
    static final String DATABASE_TABLE = "dailyschedule";
    static final int DATABASE_VERSION = 1;

    static final String DATABASE_CREATE =
            "create table dailyschedule (_id integer primary key autoincrement, " +
                    "workoutid not null, " +
                    "date text);";
                    // due to issues testing/creating the table, removing the foreign key constraint for now
                    //" FOREIGN KEY (workoutid) REFERENCES workouts (_id)); ";

    static final String DATABASE_INSERT_TEST =  "insert into dailyschedule (workoutid, date) " +
            "values (1, '26-10-2014');";

    static final String DATABASE_DELETE_INITIAL = "delete from dailyschedule where date = '26102014' and workoutid = 1;";

    final Context context;

    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    public ScheduleDBAdapter(Context ctx)
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS dailyschedule");
            onCreate(db);
        }
    }

    //---opens the database---
    public ScheduleDBAdapter open() throws SQLException
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---
    public void close()
    {
        DBHelper.close();
    }

//    public void insertInitialWorkoutDay(){
//        db.execSQL(DATABASE_DELETE_INITIAL);
//        db.execSQL(DATABASE_INSERT_TEST);
//    }

    public long insertWorkoutDay(Workout w, String day)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_WORKOUTID, w.getId());
        initialValues.put(KEY_DATE, day);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    public boolean deleteWorkoutDay(String date)
    {
        return db.delete(DATABASE_TABLE, KEY_DATE + "='" + date + "'", null) > 0;
    }

    // retrieve a particular day
    public Cursor getWorkoutDay(String date) throws SQLException
    {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                                KEY_WORKOUTID, KEY_DATE},
                        KEY_DATE + "='" + date + "'", null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
}
