package com.example.nathan.fitnesstracker;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by jordon on 2014-10-25.
 */
public class GetGymIcon extends AsyncTask<String, Void, Bitmap> {

    GymInfo gymInfo;

    public GetGymIcon(GymInfo g) {
        this.gymInfo = g;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        String getUrl = gymInfo.getIcon();
        Bitmap icon = null;
        try {
            InputStream in = new URL(getUrl).openStream();
            icon = BitmapFactory.decodeStream(in);
        } catch (MalformedURLException e) {
            Log.e("URL Error: ", e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("IO Error: ", e.getMessage());
            e.printStackTrace();
        }
        return icon;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        gymInfo.setIconBitmap(result);
    }
}