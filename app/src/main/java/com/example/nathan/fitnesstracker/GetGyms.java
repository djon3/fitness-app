package com.example.nathan.fitnesstracker;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by jordon on 2014-10-25.
 */
public class GetGyms extends AsyncTask<String, Void, String> {

    private AsyncGetGymsListener<String> callback;

    public GetGyms(AsyncGetGymsListener<String> cb) {
        this.callback = cb;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.onTaskComplete(result);
    }

    @Override
    protected String doInBackground(String... placesURL) {

        StringBuilder placesBuilder = new StringBuilder();

        for (String placeSearchURL : placesURL) {
            HttpClient placesClient = new DefaultHttpClient();
            try {
                HttpGet placesGet = new HttpGet(placeSearchURL);
                HttpResponse placesResponse = placesClient.execute(placesGet);

                StatusLine placeSearchStatus = placesResponse.getStatusLine();
                if (placeSearchStatus.getStatusCode() == 200) {
                    HttpEntity placesEntity = placesResponse.getEntity();

                    InputStream placesContent = placesEntity.getContent();
                    InputStreamReader placesInput = new InputStreamReader(placesContent);

                    BufferedReader placesReader = new BufferedReader(placesInput);
                    String lineIn;
                    while ((lineIn = placesReader.readLine()) != null) {
                        placesBuilder.append(lineIn);
                    }
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        }
        return placesBuilder.toString();
    }
}

interface AsyncGetGymsListener<T> {
    public void onTaskComplete(T result);
}