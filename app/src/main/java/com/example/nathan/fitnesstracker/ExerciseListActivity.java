package com.example.nathan.fitnesstracker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ExerciseListActivity extends NavigationActivity {

    private ListView lvItems;
    private ArrayAdapter<String> listAdapter;
    private ExercisesDBAdapter db = new ExercisesDBAdapter(this);
    private Exercise exercise = new Exercise();
    private ArrayList<Exercise> exerciseArray;
    private Exercise e = new Exercise();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_exercise_list);
        super.onCreate(savedInstanceState);
        super.setCurrentActivity(ExerciseListActivity.class);
        db.open();
        db.insertInitialExercises();
        db.close();
        populateListView();
        registerClickCallBack();

    }


    private void populateListView() {

        db.open();

        ArrayList<Exercise> exercises = db.getAllExercises();
        if (!exercises.isEmpty()) {

            List<String> str = new ArrayList<String>();
            for (Exercise e : exercises) {
                str.add(e.getName());
            }
            listAdapter = new ArrayAdapter<String>(this, R.layout.exercise_item, str);
            lvItems = (ListView) findViewById(R.id.lvExerciseItem);
            lvItems.setAdapter(listAdapter);

        }
        db.close();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.exercise_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void registerClickCallBack() {

        lvItems = (ListView) findViewById(R.id.lvExerciseItem);
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                db.open();
                exerciseArray = db.getAllExercises();

                e = exerciseArray.get(position);

                exercise.setId(e.getId());
                exercise.setName(e.getName());
                exercise.setReps(e.getReps());
                exercise.setWeight(e.getWeight());
                exercise.setTime(e.getTime());
                exercise.setDescription(e.getDescription());

                db.close();
            }
        });


    }

    public void removeExercise(){
        db.open();

        if (db.deleteExercise(e.getId())) {
            Toast.makeText(this, "Delete successful.", Toast.LENGTH_LONG).show();
            listAdapter.clear();
            populateListView();
            listAdapter.notifyDataSetChanged();
        } else
            Toast.makeText(this, "Delete failed.", Toast.LENGTH_LONG).show();
        db.close();
    }

    public void onButtonClick(View view) {
        Intent i;
        switch (view.getId()) {

            case R.id.buttonAddNew:
                i = new Intent(this, ExerciseEditorActivity.class);
                int result = 0;
                startActivityForResult(i, result);
                break;
            case R.id.buttonUpdate:
                i = new Intent(this, ExerciseEditorActivity.class);
                i.putExtra("exercise", exercise);
                result = 0;
                startActivityForResult(i, result);
                break;

            case R.id.buttonRemove:
                removeExercise();
                break;
            case R.id.buttonDone:
                finish();
                break;
            default:
                Toast.makeText(getApplicationContext(), "Unknown call to onButtonClick", Toast.LENGTH_LONG).show();
                break;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        db.open();
        db.insertInitialExercises();
        db.close();
        populateListView();
        registerClickCallBack();
    }


}
